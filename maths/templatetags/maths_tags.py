from django import template

from ..models import MathPiece

register = template.Library()

@register.inclusion_tag('maths/tags/math_pieces.html', takes_context=True)
def mathematique_pieces(context):
    return {
            'mathpieces': MathPiece.objects.all(),
        'request': context['request'],
    }
