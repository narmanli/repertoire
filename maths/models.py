from io import BytesIO
from tempfile import NamedTemporaryFile

from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.db import models
from django.db.models.fields.files import FileField, ImageFieldFile
from django.forms.utils import ErrorList
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django_tex.core import compile_template_to_pdf
from pdf2image import convert_from_bytes
from PIL import Image
from pypandoc import convert_text
from wagtail.admin.edit_handlers import StreamFieldPanel, FieldPanel
from wagtail.core import blocks
from wagtail.core.fields import StreamField
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.images.blocks import ImageChooserBlock
from wagtail.snippets.blocks import SnippetChooserBlock
from wagtail.contrib.settings.models import BaseSetting, register_setting


def latex_to_html(latex, raw=False):
    pdoc_args = ["--mathjax"]
    if raw:
        form = "latex-latex_macros"
    else:
        form = "latex"
    output = convert_text(latex, "html", format="latex", extra_args=pdoc_args)
    return output


@register_setting(icon="code")
class LateXMathSettings(BaseSetting):
    preambule = models.TextField(
        _("préambule LaTeX commun"),
        help_text=mark_safe("""Le contenu sera utilisé comme préambule LaTeX pour l'affichage Web de tous les exercices et comme partie commune des préambule des feuilles d'exercice. Les commandes <code>\def</code>, <code>\\newcommand</code>, <code>\\renewcommand</code>, <code>\\newenvironment</code>, <code>\\renewenvironment</code>, et <code>\\let</code> peuvent être utilisées. Plus d'informations <a href="https://docs.mathjax.org/en/latest/tex.html#defining-tex-macros">sur cette page</a>"""),
    )
    html_preambule = models.TextField(_("préambule compilé en html"), editable=False)

    panels = [FieldPanel("preambule")]

    def save(self, *args, **kwargs):
        # On doit mettre les commandes dans des maths sinon pandoc les retire...
        embendded_preambule = "$" + self.preambule + "$"
        self.html_preambule = latex_to_html(embendded_preambule, raw=True)
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = "Maths en LaTeX"


class LatexBlock(blocks.StructBlock):
    latex = blocks.TextBlock()
    html = blocks.TextBlock(required=False)

    def get_form_context(self, value, prefix="", errors=None):
        context = super().get_form_context(value, prefix=prefix, errors=errors)
        context["hidden_fields"] = ["html"]
        return context

    def clean(self, value):
        # build up a list of (name, value) tuples to be passed to the StructValue constructor
        result = []
        errors = {}
        # value = blocks.StructValue([('latex', 'dsqdsdsqdq maths'), ('html', 'je suis un caca')])
        for name, val in value.items():
            try:
                result.append((name, self.child_blocks[name].clean(val)))
            except ValidationError as e:
                errors[name] = ErrorList([e])

        if errors:
            # The message here is arbitrary - StructBlock.render_form will suppress it
            # and delegate the errors contained in the 'params' dict to the child blocks instead
            raise ValidationError("Validation error in StructBlock", params=errors)

        latex = ""
        for name, val in result:
            if name == "latex":
                latex = val

        new_result = []
        for name, val in result:
            if name == "html":
                val = latex_to_html(latex)
            new_result.append((name, val))

        return self._to_struct_value(new_result)

    class Meta:
        template = "maths/blocks/latex.html"
        form_template = "maths/admin/struct_hide_field.html"
        icon = "pilcrow"
        help_text = _("Bloc de texte mathématiques écrit en LaTeX")


class MathDraw(models.Model):
    fichier = models.ImageField(_("fichier"), upload_to="MathDraw")

    class Meta:
        verbose_name = _("dessin")
        verbose_name_plural = _("dessins")

    def __str__(self):
        return "Je suis un Dessin de maths"


class TikzBlock(blocks.StructBlock):
    tikz = blocks.TextBlock()
    image = SnippetChooserBlock(MathDraw, required=False)
    caption = blocks.CharBlock(label=_("description"), max_length=400)

    def get_form_context(self, value, prefix="", errors=None):
        context = super().get_form_context(value, prefix=prefix, errors=errors)
        context["hidden_fields"] = ["image"]
        return context

    def clean(self, value):
        result = (
            []
        )  # build up a list of (name, value) tuples to be passed to the StructValue constructor
        errors = {}
        # value = blocks.StructValue([('latex', 'dsqdsdsqdq maths'), ('html', 'je suis un caca')])
        for name, val in value.items():
            try:
                result.append((name, self.child_blocks[name].clean(val)))
            except ValidationError as e:
                errors[name] = ErrorList([e])

        if errors:
            # The message here is arbitrary - StructBlock.render_form will suppress it
            # and delegate the errors contained in the 'params' dict to the child blocks instead
            raise ValidationError("Validation error in StructBlock", params=errors)

        # ON RÉCUPÈRE LES VALEURS ET INDEX QUI NOUS INTÉRESSENT DANS result
        tikz = "erreur ?"
        for name, value in result:
            if name == "tikz":
                tikz = value
            elif name == "image":
                draw = value

        # ON TRAVAILLE
        # On compile le TikZ
        template_name = "maths/tex/standalone.tex"
        context = {"code": tikz}
        pdf = compile_template_to_pdf(template_name, context)
        images = convert_from_bytes(pdf)
        image = images[0]  # on prend la première page

        # La solution pour convertir en png est pour le moment de
        # sauvegarder en png dans un tempfile et de l'ouvir ensuite :
        f = NamedTemporaryFile(suffix=".png")
        image.save(f)
        image_png = Image.open(f)

        filename = "tikz-block.png"

        # On modifie le résultat
        if draw is None:
            fichier = ImageFieldFile(
                instance=None, field=FileField(), name="im an error"
            )
            draw = MathDraw(fichier=fichier)

        draw.fichier = filename
        tempimage = image_png
        tempimage_io = BytesIO()
        tempimage.save(tempimage_io, format=image_png.format)
        draw.fichier.save(filename, ContentFile(tempimage_io.getvalue()))

        # ON MODIFIE LE result EN CONSÉQUENCE
        new_result = []
        for name, value in result:
            if name == "image":
                value = draw
            new_result.append((name, value))

        return self._to_struct_value(new_result)

    class Meta:
        template = "maths/blocks/tikz.html"
        form_template = "maths/admin/struct_hide_field.html"
        default = {"tikz": "\\begin{tikzpicture}\n\n\\end{tikzpicture}"}
        icon = "placeholder"
        help_text = _("code TikZ pour une illustration")


class ImageBlock(blocks.StructBlock):
    image = ImageChooserBlock()
    caption = blocks.CharBlock(label=_("description"), max_length=400)
    source = DocumentChooserBlock(
        label=_("source"),
        help_text=_("fichier source de l'image, parce que c'est cool de partager :)"),
        required=False,
    )

    class Meta:
        template = "maths/blocks/image.html"
        icon = "picture"


class MathBlock(blocks.StreamBlock):
    latex = LatexBlock()
    tikz = TikzBlock()
    image = ImageBlock()

    class Meta:
        template = "maths/blocks/math.html"


class MathPiece(models.Model):
    body = StreamField(MathBlock(block_counts={"latex": {"min_num": 1}}))

    panels = [StreamFieldPanel("body")]

    class Meta:
        verbose_name = _("maths")
        verbose_name_plural = _("maths")

    def __str__(self):
        return "Je suis des maths"
