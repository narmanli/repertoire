(function(){
	// Get the elements.
	// - the 'pre' element.
  var pre = document.getElementsByTagName('pre');

	// Add a copy button in the 'pre' element.
	// which only has the className of 'language-'.
  for (var i = 0; i < pre.length; i++) {
      var isLanguage = pre[i].children[0].className.indexOf('language-');
      if ( isLanguage === 0 ) {
              var button           = document.createElement('button');
                      button.className = 'copy-button';
                      button.textContent = 'Copier';

                      pre[i].appendChild(button);
          }
  }


	// Run Clipboard
  var clipboard = new ClipboardJS('.copy-button', {
      target: function(trigger) {
          return trigger.previousElementSibling;
      }
  });


	// On success:
	// - Change the "Copy" text to "Copied".
	// - Swap it to "Copy" in 2s.
  clipboard.on('success', function(event) {
      event.clearSelection();

      event.trigger.textContent = 'Copié !';
      window.setTimeout(function() {
        event.trigger.textContent = 'Copier';
      }, 2000);
  });

	// On error (Safari):
	// - Change the  "Press Ctrl+C to copy"
	// - Swap it to "Copy" in 2s.
	
	clipboard.on('error', function(event) { 
		event.trigger.textContent = 'Press "Ctrl + C" to copy';
		window.setTimeout(function() {
			event.trigger.textContent = 'Copier';
		}, 5000);
	});

})();

