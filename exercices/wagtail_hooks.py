from django.utils.translation import ugettext_lazy as _
from wagtail.contrib.modeladmin.options import (ModelAdmin, ModelAdminGroup,
                                                modeladmin_register)

from .models import Chapter, Exercice, SchoolLevel, Source, StudyBranch


class StudyBranchAdmin(ModelAdmin):
    model = StudyBranch
    menu_label = _("Branches")
    menu_icon = "snippet"
    list_display = ("name", "number_chapters")

    def number_chapters(self, obj):
        return obj.chapters.count()

    number_chapters.short_description = _("# chaptre(s)")


class SchoolLevelAdmin(ModelAdmin):
    model = SchoolLevel
    menu_icon = "order"
    list_display = ("title", "number_chapters", "number_exercices",)


class SourceAdmin(ModelAdmin):
    model = Source
    menu_icon = "collapse-down"
    list_display = ("title", "number_exercices", "author", "link")
    search_fields = ("title", "author", "link")


class ChapterAdmin(ModelAdmin):
    model = Chapter
    menu_icon = "list-ul"
    list_display = ("title", "level", "branch", "number_exercices")


class ExoDataAdminGroup(ModelAdminGroup):
    menu_label = _("Métadonnées")
    menu_order = 300
    items = (SchoolLevelAdmin, ChapterAdmin, SourceAdmin, StudyBranchAdmin)


class ExerciceAdmin(ModelAdmin):
    model = Exercice
    menu_icon = "form"  # change as required
    menu_order = 200  # (000 being 1st, 100 2nd)
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ("__str__", "chapter", "difficulty")
    list_filter = ("chapter", "difficulty", "sources",)


modeladmin_register(ExerciceAdmin)
modeladmin_register(ExoDataAdminGroup)
