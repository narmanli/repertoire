from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import RandomCharField
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.models import ClusterableModel
from taggit.models import TaggedItemBase
from wagtail.admin.edit_handlers import (
    FieldPanel,
    FieldRowPanel,
    HelpPanel,
    InlinePanel,
    MultiFieldPanel,
    ObjectList,
    PageChooserPanel,
    StreamFieldPanel,
    TabbedInterface,
)
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Orderable, Page
from wagtailautocomplete.edit_handlers import AutocompletePanel

from maths.models import MathBlock, MathPiece

DIFFICULTY_CHOICES = (
    (1, "1 : Facile"),
    (2, "2"),
    (3, "3"),
    (4, "4"),
    (5, "5 : Difficile"),
)

LICENCE_CHOICES = (
    ("CC-Zero", "Zéro"),
    ("CC-BY", "Attribution"),
    ("CC-BY-SA", "Attribution, Partage des conditions" " initiales à l'identique"),
    ("CC-BY-ND", "Attribution, Pas de modification"),
    ("CC-BY-NC", "Attribution, Pas d'utilisation commerciale"),
    (
        "CC-BY-NC-SA",
        "Attribution, Pas d'utilisation commerciale,"
        " Partage des conditions initiales à l'identique",
    ),
    (
        "CC-BY-NC-ND",
        "Attribution, Pas d'utilisation commerciale," " Pas de modification",
    ),
    # ("GPL", "Licence publique générale GNU"),
    # ("LGPL", "Licence publique générale limitée GNU"),
)

LICENCE_CHOICES_ICONS = {
    "CC-Zero": """<i class="fab fa-creative-commons-zero"></i>""",
    "CC-BY": """<i class="fab fa-creative-commons-by"></i>""",
    "CC-BY-SA": """<i class="fab fa-creative-commons-by"></i><i class="fab fa-creative-commons-sa"></i>""",
    "CC-BY-ND": """<i class="fab fa-creative-commons-by"></i><i class="fab fa-creative-commons-nd"></i>""",
    "CC-BY-NC": """<i class="fab fa-creative-commons-by"></i><i class="fab fa-creative-commons-nc-eu"></i>""",
    "CC-BY-NC-SA": """<i class="fab fa-creative-commons-by"></i><i class="fab fa-creative-commons-nc-eu"></i><i class="fab fa-creative-commons-sa"></i>""",
    "CC-BY-NC-ND": """<i class="fab fa-creative-commons-by"></i><i class="fab fa-creative-commons-nc-eu"></i><i class="fab fa-creative-commons-nd"></i>""",
    # ("GPL", "Licence publique générale GNU",
    # ("LGPL", "Licence publique générale limitée GNU",
}


class StudyBranch(models.Model):
    name = models.CharField(_("nom"), max_length=200)

    autocomplete_search_field = "name"

    def autocomplete_label(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "branche de la discipline"
        verbose_name_plural = "branches de la discipline"


class SchoolLevel(Page):
    # title inherited from Page as the SchoolLevel.title
    before_menu = RichTextField(blank=True)
    after_menu = RichTextField(blank=True)
    aside = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel("before_menu", classname="full"),
        HelpPanel(
            content="Ici sera inséré le menu vers les différents chapitre qui correspondent à ce niveau"
        ),
        FieldPanel("after_menu", classname="full"),
        FieldPanel("aside", classname="full"),
    ]

    parent_page_types = ["home.HomePage"]
    subpage_types = ["Chapter", "Concours"]

    def number_chapters(self):
        return self.get_descendants().exact_type(Chapter).count()

    number_chapters.short_description = _("# chaptre(s)")

    def number_exercices(self):
        chapters = self.get_descendants().exact_type(Chapter)
        return Exercice.objects.filter(chapter__in=chapters).count()

    number_exercices.short_description = _("# exo(s)")

    def get_context(self, request):
        context = super().get_context(request)

        context["sorted_chapters"] = (
            Chapter.objects.child_of(self).live().order_by("branch__name")
        )
        return context

    class Meta:
        verbose_name = "niveau scolaire"
        verbose_name_plural = "niveaux scolaires"


class Chapter(Page):
    # title inherited from Page as the Chapter.title
    branch = models.ForeignKey(
        StudyBranch, on_delete=models.PROTECT, related_name="chapters"
    )

    parent_page_types = ["SchoolLevel", "Chapter"]
    subpage_types = ["Chapter"]

    content_panels = Page.content_panels + [
        AutocompletePanel(
            "branch", target_model="exercices.StudyBranch", is_single=True
        ),
        # PageChooserPanel("level"),
    ]

    def number_exercices(self):
        return self.exercices.count()

    number_exercices.short_description = _("# exo(s)")

    def level(self):
        return self.get_ancestors().exact_type(SchoolLevel).first()

    def get_context(self, request):
        context = super().get_context(request)

        sub_chapters = self.get_children().exact_type(Chapter)
        descendants = self.get_descendants()
        descendants_ids = [child.id for child in descendants]
        descendants_ids.append(self.id)
        context["sub_chapters"] = sub_chapters
        context["exercices"] = Exercice.objects.filter(chapter__id__in=descendants_ids)
        return context

    def small_name(self):
        parent = self.get_parent()
        level = self.level()
        res = ""
        if isinstance(parent.specific, Chapter):
            res = "[{}] … > {}".format(level.title, self.title)
        else:
            res = "[{}] {}".format(level.title, self.title)

        if self.get_children().exists():
            res += " > …"
        return res

    def autocomplete_label(self):
        return self.small_name()

    def __str__(self):
        return self.small_name()

    class Meta:
        verbose_name = "chapitre"
        verbose_name_plural = "chapitres"


class Concours(Page):
    # title inherited from Page as the Chapter.title
    parent_page_types = ["SchoolLevel"]
    subpage_types = []

    def __str__(self):
        return "[{}] {}".format(self.title, self.get_parent().title)

    class Meta:
        verbose_name = "concours"
        verbose_name_plural = "concours"


class Source(models.Model):
    title = models.CharField(_("titre"), max_length=300)
    author = models.CharField(_("aut·rice·eur·s"), max_length=300, blank=True)
    link = models.URLField(_("lien"), max_length=300, blank=True)

    def number_exercices(self):
        return self.sourced_exercices.count()

    number_exercices.short_description = _("# exo(s)")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "source"
        verbose_name_plural = "sources"


class NotionTag(TaggedItemBase):
    content_object = ParentalKey(
        "exercices.Exercice", on_delete=models.CASCADE, related_name="related_exercices"
    )


class Exercice(ClusterableModel):
    name = models.CharField(_("intitulé"), max_length=200, blank=True)
    slug = RandomCharField(length=6, include_alpha=False)
    enonce = StreamField(MathBlock(block_counts={"latex": {"min_num": 1}}))
    # solutions via foreignkey
    chapter = ParentalManyToManyField(Chapter, related_name="exercices")
    # niveau via chapter
    licence = models.CharField(
        _("licence"), max_length=200, choices=LICENCE_CHOICES, default="CC-Zero"
    )
    sources = ParentalManyToManyField(
        Source, blank=True, related_name="sourced_exercices"
    )
    notions = ClusterTaggableManager(
        _("notions"), through=NotionTag, blank=True, related_name="related_exercices"
    )
    concours = ParentalManyToManyField(Concours, blank=True)
    difficulty = models.PositiveSmallIntegerField(
        _("difficulté"), choices=DIFFICULTY_CHOICES
    )

    # TODO search_fields

    content_panels = [
        StreamFieldPanel("enonce", classname="collapsible"),
        InlinePanel("solutions", label="Solution(s)", classname="collapsible"),
    ]

    meta_panels = [
        MultiFieldPanel(
            [
                FieldPanel("name"),
                AutocompletePanel(
                    "chapter", target_model="exercices.Chapter", is_single=False
                ),
                FieldPanel("difficulty"),
                FieldPanel("notions"),
                AutocompletePanel(
                    "concours", target_model="exercices.Concours", is_single=False
                ),
            ],
            heading="retrouver l'exercice",
        ),
        MultiFieldPanel(
            [
                FieldPanel("licence"),
                AutocompletePanel(
                    "sources", target_model="exercices.Source", is_single=False
                ),
            ],
            heading="informations",
        ),
    ]
    edit_handler = TabbedInterface(
        [
            ObjectList(content_panels, heading="Exercice"),
            ObjectList(meta_panels, heading="Métadonnées"),
        ]
    )

    def __str__(self):
        res = ""
        if self.name != "":
            res += self.name + " | "
        for block in self.enonce:
            if block.block_type == "latex":
                res += block.value.get("latex")[0:100]
                if len(res) >= 100:
                    res = res[0:100] + "…"
                break
        return res

    def licence_icon(self):
        return LICENCE_CHOICES_ICONS[self.licence]

    class Meta:
        verbose_name = "exercice"
        verbose_name_plural = "exercices"


class Solution(Orderable):
    exercice = ParentalKey(
        "exercices.Exercice", on_delete=models.CASCADE, related_name="solutions"
    )
    solution = StreamField(MathBlock(block_counts={"latex": {"min_num": 1}}))

    panels = [StreamFieldPanel("solution", classname="full")]

    class Meta:
        verbose_name = "solution"
        verbose_name_plural = "solutions"
