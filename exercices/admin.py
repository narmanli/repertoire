from django.contrib import admin

from .models import StudyBranch


class MathBranchAdmin(admin.ModelAdmin):
    list_display = ["name"]
    ordering = ["name"]
    search_fields = ["name"]

    save_as_continue = True
    save_on_top = True

admin.site.register(StudyBranch, MathBranchAdmin)
