from django.db import models

from wagtail.core.blocks import ListBlock, RichTextBlock, PageChooserBlock
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel

from exercices.models import SchoolLevel, Concours


class HomePage(Page):
    body = StreamField([
            ('paragraph', RichTextBlock()),
            ('levelMenu', ListBlock(
                    PageChooserBlock(
                        SchoolLevel
                ))),
            ('concoursMenu', ListBlock(
                    PageChooserBlock(
                        Concours
                ))),
        ], blank=True)
    aside = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        StreamFieldPanel('body', classname="full"),
        FieldPanel('aside', classname="full"),
    ]
